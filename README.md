# WIKI Système de design du Ministère de la Justice | DSMJ

## Description
Le Wiki DSMJ est un référentiel de composants du Système de design de l'état (i.e. DSFR) avec une surcharge du framework Angular et, pour certains composants, une custumisation, c'est-à-dire comportant un ajout de fonctionnalité(s) aux composants natifs du DSFR. Ces composants spécifiques DSMJ sont conformes au Référentiel général d'amélioration de l'accessibilité (i.e. RGAA version 4.1) et respectent les fondamentaux du DSFR.



Lexique

- Composant\
Elément d'interface plus ou moins complexe (par ex. une barre de recherche avec un champ de saisie et son bouton Rechercher; une entête/header), réutilisable pas les designers et les développeurs afin de concevoir et développer une application métier ou toute autre frome de produit ou service digital. On parle désormais d'Atomic design (Brad Frost) car on ne conçoit plus des pages de site mais des systèmes de composants en partant de la plus simple unité atomique jusqu'à l'interface web : une icône/Atome; une barre de recherche/Molécule ou combinaison d'atomes; une entête/Organisme ou groupe de molécules; une stucture d'IHM/ Template ou assemblage de groupes d'organismes.\
https://bradfrost.com/blog/post/atomic-web-design/#templates

- Système de design / Design system \
Bibliothèque de composants personnalisés clé en main, prêts à l'emploi et écrits dans un langage commun. Le design system propose des composants graphiques et leur versant codés en html/css/js et Angular à usage des designers et développeurs.

- Système de design de l'état | DSFR \
Porté et animé par le Service d’Information du Gouvernement, le DSFR représente l’identité numérique de l’État et vise à améliorer la relation numérique État / citoyen grâce à une interface commune, accessible, lisible et comprise par tous. Le système de design de l’État ainsi que les principes qui le sous-tendent visent à créer une interface et un site pour répondre aux besoins des designers et des développeurs tout au long de leur projet.\
https://gouvfr.atlassian.net/wiki/spaces/DB/overview?homepageId=145359476


